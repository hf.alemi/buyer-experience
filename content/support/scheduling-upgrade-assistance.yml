---
title: Scheduling Upgrade Assistance
description: Upgrade Assistance FAQ
support-hero:
  data:
    title: Scheduling Upgrade Assistance
    content: |
      The GitLab support team is here to help. As a part of our <a href="/support/#priority-support">Priority Support</a> we offer Upgrade Assistance. 
      That is, we'll review your upgrade and rollback plans and provide 
      feedback and suggestions to help increase the likelihood of a smooth upgrade.
side_menu:
  anchors:
    text: 'ON THIS PAGE'
    data:
      - text: 'What is "Upgrade Assistance"?'
        href: '#what-is-upgrade-assistance'
      - text: "What information do I need to schedule Upgrade Assistance?"
        href: "#what-information-do-i-need-to-schedule-upgrade-assistance"
      - text: "How far in advance must I open a Support Request to request Upgrade Assistance?"
        href: "#how-far-in-advance-must-i-open-a-support-request-to-request-upgrade-assistance"
      - text: "Can we use custom scripts for upgrade/rollback plans?"
        href: "#can-we-use-custom-scripts-for-upgrade-rollback-plans"
      - text: "How do I schedule Upgrade Assistance?"
        href: "#how-do-i-schedule-upgrade-assistance"
        nodes:
          - text: 'Can I record the upgrade plan review session?'
            href: '#can-i-record-the-upgrade-plan-review-session'
          - text: 'Global Support'
            href: '#global-support'
          - text: 'US Federal Support'
            href: '#us-federal-support'                                   
      - text: "What does Upgrade Assistance Look like?"
        href: "#what-does-upgrade-assistance-look-like"
      - text: "What versions of GitLab will you support upgrading between?"
        href: "#what-versions-of-gitlab-will-you-support-upgrading-between"
      - text: "What if I don't give sufficient notice? Will I still be supported?"
        href: "#what-if-i-dont-give-sufficient-notice-will-i-still-be-supported"                                          
  hyperlinks:
    text: ''
    data: []
components:
- name: support-copy
  data:
    block:
    - header: What is "Upgrade Assistance"? 
      id: what-is-upgrade-assistance
      text: |
        <p>
          Upgrade Assistance is part of the Priority Support package that lets you work directly with a GitLab Support Engineer during the planning 
          of a self-hosted GitLab instance upgrade between release versions. As part of Upgrade Assistance, a GitLab Support Engineer will:
        </p>
        <ul>
          <li>Review and provide feedback on the upgrade plan you provide</li>
          <li>Review and provide feedback on the rollback plan you provide</li>
          <li><strong>Optionally</strong> you may request the engineer host a 30 minute screen share session to review the upgrade and rollback plans synchronously with you. 
            This must be requested and <a href="/support/scheduling-upgrade-assistance/#how-far-in-advance-must-i-open-a-support-request-to-request-upgrade-assistance">scheduled with the engineer in advance</a>. 
            The purpose of the call is to provide final review and help familiarize you with 
            the steps prior to initiating the upgrade. Engineers will not be engaged synchronously during the upgrade timeframe.
          </li>
        </ul>
        <p>
          "Upgrade" in this sense does not include:
        </p>
        <ul>
          <li>migrations between GitLab distributions (e.g. GitLab CE to GitLab EE)</li>
          <li>migrations between GitLab architectures (e.g. GitLab Omnibus to GitLab Helm)</li>
          <li>scaling current architecture (e.g. GitLab 5k Reference architecture to GitLab 20k Reference Architecture)</li>
          <li>moving data between GitLab installations (e.g. migrating from GitLab self-managed to GitLab.com)</li>
          <li>adding additional GitLab features (e.g. adding a GitLab Geo replica)</li>
        </ul>
        <p>
          In short, Upgrade Assistance is simply for moving from one GitLab point release to a newer GitLab point release on the same server.
        </p>
    - header: What information do I need to schedule Upgrade Assistance?
      id: what-information-do-i-need-to-schedule-upgrade-assistance
      text: |
        <p> 
          First, confirm that nothing about your instance would make the request <a href="/support/statement-of-support.html#out-of-scope">out of scope for support</a>.
        </p>
        <br />
        <p>
          Then, please provide all the relevant information you can so that we will be best positioned to assist you. 
          At a minimum, we require:
        </p>
        <ol>
          <li>An <a href="https://docs.gitlab.com/ee/update/plan_your_upgrade.html#upgrade-plan">upgrade plan</a></li>
          <li>A <a href="https://docs.gitlab.com/ee/update/plan_your_upgrade.html#rollback-plan">rollback plan</a></li>
          <li>Updated architecture documentation</li>
          <li>The point of contact for support to use (email address preferred)</li>
          <li>The time of the upgrade (please include date, time, and timezone)</li>
          <li>Any additional relevant information (e.g. We've had no issues simulating this upgrade in our staging environment)</li>
        </ol>
        <p>
           See the <a href="https://docs.gitlab.com/ee/update/plan_your_upgrade.html">create a GitLab upgrade plan</a> 
           documentation for further advice on planning an upgrade.
        </p>
    - header: How far in advance must I open a Support Request to request Upgrade Assistance?
      id: how-far-in-advance-must-i-open-a-support-request-to-request-upgrade-assistance
      text: |
        <p>
          For support to properly assist you, the earlier you can notify us and include all of the information we need, the better. 
          After providing the necessary information needed in order to request Upgrade Assistance, our minimum requirements for notification are:
        </p>
        <ul>
          <li>
            Upgrades should be scheduled at least one week after support has confirmed they have received all relevant upgrade documents.
          </li>
          <li>
            Two weeks notice once all relevant upgrade documents are received is required for all upgrades involving the optional 30 minute synchronous upgrade plan review session
          </li>
        </ul>
        <p>
          If you cannot meet our minimum advanced notice period for your planned upgrade, we may recommend postponing the upgrade attempt. If you choose to proceed with the originally 
          scheduled date:
        </p>
        <ul>
          <li>Should time permit we will get a support engineer to review your upgrade and rollback plans.</li>
          <li>You still have access to emergency support should you encounter a production outage as a result of your upgrade.</li>
        </ul>
    - header: Can we use custom scripts for upgrade/rollback plans?
      id: can-we-use-custom-scripts-for-upgrade-rollback-plans
      text: |
        <p>
          You can do so, however we cannot review the scripts themselves to determine if they are viable. Generally speaking, our upgrade documentation is the single source of 
          truth for how to best carry out an upgrade.
        </p>
        <br />
        <p>
          If issues do occur during the upgrade window and you are running a custom script, it is likely the advice from support will be to utilize your rollback plan. 
          It is important to know that should issues arise while using custom scripts, Support will recommend following the exact steps from our documentation on future attempts.
        </p>
    - header: How do I schedule Upgrade Assistance? 
      id: how-do-i-schedule-upgrade-assistance
      subtitle: 
        text: Can I record the upgrade plan review session?
        id: can-i-record-the-upgrade-plan-review-session
      text: |
        <p>
          Frequently during screenshare sessions plaintext secrets or other sensitive information can be displayed. To ensure sure that any recordings that inadvertantly 
          contain this information stay within your security boundary, we ask that customers initiate and store any recordings.
        </p>
        <br />
        <p>
          If you wish to record the session, either request the engineer(host) to grant you the ownership of the call to start the recording process or invite the respective engineer 
          to a call where you can initiate the recording from your end.
        </p>
    - subtitle:
        text: Global Support
        id: global-support
      text: |
        <p>
          Organizations with <a href="/support/#priority-support">Priority Support</a> or higher may use the <a href="https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=426148">Support for Self Managed instances</a> 
          form and under "Problem Type" select "Upgrade Assistance Request" to begin the process. 
          For efficiency, please include the <a href="/support/scheduling-upgrade-assistance/#what-information-do-i-need-to-schedule-upgrade-assistance">required information</a> when opening the ticket.
        </p>
    - subtitle:
        text: US Federal Support
        id: us-federal-support
      text: |
        <p>
          If your organization meets the <a href="/support/#limitations">requirements</a> for GitLab's US Federal Support you may use the 
          <a href="https://federal-support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001434131">US Federal Upgrade Assistance Request</a> form to begin the process. 
          For efficiency, please include the <a href="/support/scheduling-upgrade-assistance/#what-information-do-i-need-to-schedule-upgrade-assistance">required information</a> when opening the ticket.
        </p>
    - header: What does Upgrade Assistance Look like?
      id: what-does-upgrade-assistance-look-like
      text: |
        <p>
          Once a case is filed with the relevant support team it will be triaged by a support engineer who will verify that all of the requested information was provided. The engineer will then review the upgrade, rollback, and architecture plans, 
          making suggestions or notes on additional steps or other concerns to be aware of. If requested, the engineer can provide a single use Calendly link to schedule a review call <strong>in advance</strong> of when you plan to run your upgrade to answer any 
          final questions you may have. Note that the review call will not be offered until the engineer has all of the available information required. We recommend scheduling the review call at least 3 business days ahead of your scheduled upgrade. 
          Please be as comprehensive as possible when opening the ticket to avoid delays.
        </p>
        <br />
        <p>
          After confirming that all relevant information has been provided, and thorough review has been completed with all questions and concerns addressed you should be ready to begin your GitLab upgrade!
        </p>
        <br />
        <p>
          If you have requested the optional 30 minute upgrade plan review session with an engineer, a GitLab Support Engineer will join you at the scheduled meeting time to help kick things off and ensure that you're set up for success by:
        </p>
        <ol>
          <li>Ensuring that there's an open ticket for the upgrade that they are monitoring</li>
          <li>Going over the upgrade plan once more</li>
          <li>Verifying that there is a rollback plan in place should things not go according to plan.</li>
        </ol>
        <p>
          After 30 minutes the engineer will drop off the call and may be available via the case asynchronously for any questions that may arise.
        </p>
        <p>
          Once the upgrade is complete, and has passed your post-upgrade success criteria, please be sure to send an update to the ticket that was opened so the Engineer 
          knows they can resolve the case.
        </p>
        <p>
          If there haven't been any updates for some time, the Engineer assisting may send an update to the ticket requesting a status check in.
        </p>
        <p>
          If you run into a production impacting issue while performing your upgrade and cannot proceed you may <a href="/support/#how-to-trigger-emergency-support">page the on-call engineer</a>. 
          The on-call engineer may invoke your rollback plan in order to avoid further impact to production availability.
        </p>
    - header: What versions of GitLab will you support upgrading between?
      id: what-versions-of-gitlab-will-you-support-upgrading-between
      text: |
        <p>
          As noted in our Statement of Support, we <a href="/support/scheduling-upgrade-assistance/statement-of-support.html#we-support-the-current-major-version-and-the-two-previous-major-versions">support the current major version and two previous major versions</a>. 
          If you're upgrading from a version of GitLab that is no longer supported, Upgrade Assistance may not be an option. 
          If you're in this situation, please discuss options with your Technical Account Manager or your Account Manager for Professional Services options.
        </p>
    - header: What if I don't give sufficient notice? Will I still be supported?
      id: what-if-i-dont-give-sufficient-notice-will-i-still-be-supported
      text: |
        <p>
          As a part of <a href="/support/#priority-support">Priority Support</a>, you're also entitled to <strong>24x7 uptime Support</strong>. If you encounter any issues that are leading to downtime during your upgrade, you can page the on-call engineer to help troubleshoot.
        </p>
        <br />
        <p>
          Please provide as much context as you can, including an upgrade plan when you open your emergency ticket.
        </p>
        <br />
        <p>
          Do note, that in some cases the best option available may be to invoke your rollback plan and reschedule the upgrade.
        </p>



